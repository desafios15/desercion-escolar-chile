# 002-Metodología

1- Para hacer este análisis se utilizaron fuentes gubernamentales del Ministerio de Educación de Chile (MINEDUC) y del Ministerio de Desarrollo Social y Familia.

- Se descargan los datos en formato `CSV` para el rendimiento académico desde la página oficial del [Ministerio de Educación de Chile](https://datosabiertos.mineduc.cl/rendimiento-por-estudiante-2/) y para el Índice de prioridad social (IPS) desde la página oficial del [Ministerio de desarrollo Social y Familia](https://www.desarrollosocialyfamilia.gob.cl/storage/docs/boletin_interno/INDICE_DE_PRIORIDAD_SOCIAL_2020.pdf).

2- Las bases de rendimiento escolar contienen información que viene desagregada por alumno, incoporando información sobre su rendimiento, asistencia y situación de promoción al final del año. Además se incorpora información sobre el establecimiento educacional al que pertenece. Esta base de datos es de carácter censal, considerando toda la población de estudiantes de Chile.

3- La base de IPS, contiene la información por comuna de toda la Región Metropolitana de Chile.

4.- Se define como alumno desertor, a todo estudiante cuya situación de promoción en los establecimientos finalizado el año escolar es marcada cómo Retirado (Y).

5.- El [Índice de Prioridad Social (IPS)](https://www.desarrollosocialyfamilia.gob.cl/storage/docs/INDICE-DE-PRIORIDAD-SOCIAL-2022_V2.pdf) es un indicador compuesto que integra aspectos relevantes del desarrollo social *comunal*, esto es, las dimensiones de: ingresos, educación y salud. Se trata de un índice sintético cuyo valor numérico permite dimensionar el nivel de desarrollo relativo alcanzado por la población de una comuna.

6.- Los niveles de Priorización Social son a nivel Comunal y se obtienen de las estimaciones realizadas por el Ministerio de Desarrollo Social y Familia. Los niveles de priorización por comuna son los siguientes: ALTA PRIORIDAD SOCIAL, MEDIA ALTA PRIORIDAD SOCIAL, MEDIA BAJA PRIORIDAD SOCIAL , BAJA PRIORIDAD SOCIAL y SIN PRIORIDAD SOCIAL.

7.- Para hacer este análisis se compara la proporción de alumnos retirados por establecimiento de los años 2018 y 2019 (PRE-PAND) con la misma de estudiantes de los años 2020 y 2021 (PANDEMIA).

8- Dado el gran tamaño de cada base de datos del MINEDUC (1.2 millones de registros por año), para este análisis sólo se considerarán las comunas de la Región Metropolitana de Chile.

9- La proporción de estudiantes retirados (deserción) se calcula a nivel de establecimiento. En la Región Metropolitana existen aproximadamente 2150 establecimientos educacionales.

10- Hipótesis estadísticas

Hipótesis estadística 1:

$\mathcal{H}_{0}$: La media de la proporción de deserción escolar de los establecimientos PRE-PANDEMIA es mayor a la media de la proporción de deserción escolar durante la PANDEMIA.

$\mathcal{H}_{a}$: La media de la proporción de deserción escolar de los establecimientos PRE-PANDEMIA es menor o igual a la media de la proporción de deserción escolar durante la PANDEMIA.

Hipótesis estadística 2:

$\mathcal{H}_{0}$: La media de la proporción de deserción escolar de los establecimientos con ALTA PRIORIDAD SOCIAL durante la PRE-PANDEMIA es mayor a la media de la proporción de deserción escolar durante la PANDEMIA.

$\mathcal{H}_{a}$: La media de la proporción de deserción escolar de los establecimientos con ALTA PRIORIDAD SOCIAL durante la PRE-PANDEMIA es menor o igual a la media de la proporción de deserción escolar durante la PANDEMIA.

Hipótesis estadística 3:

$\mathcal{H}_{0}$: La media de la proporción de deserción escolar de los establecimientos SIN PRIORIDAD SOCIAL durante la PRE-PANDEMIA es menor o igual a la media de la proporción de deserción escolar durante la PANDEMIA.

$\mathcal{H}_{a}$: La media de la proporción de deserción escolar de los establecimientos SIN PRIORIDAD SOCIAL durante la PRE-PANDEMIA es mayor a la media de la proporción de deserción escolar durante la PANDEMIA.

11- Los datos obtenidos en este análisis son de caracter Censal. Es decir, los estimadores son a su vez los parámetros de la población. Dado esto, no es pertinente realizar una prueba de hipótesis para comparar dos muestras. Solamente es necesario calcular los promedios (parámetro) y ver la magnitud del efecto en porcentaje.

Sin embargo, para efectos académicos, se asume que son muestras y se evalúa normalidad en la distribución de los datos de la proporción de alumnos retirados. Si existe normalidad se realiza un test de hipótesis usando un t-test. Si no se encuentra normalidad se realiza un test no paramétrico de Man Whitney.