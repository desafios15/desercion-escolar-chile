from google.cloud import bigquery
from google.oauth2 import service_account
import pandas as pd

def create_bigquery_table(data_set,table_name,df):
    """
    dataset(str): Nombre del dataset destino
    table_name(str):Nombre de la tabla destino
    df: nombre del dataframe a insertar en la tabla destino
    """
    print("Set client connection")
    project_id = 'platzi-proyect-365801'
    credentials = service_account.Credentials.from_service_account_file(filename="platzi-proyect-365801-ec703f041c6b.json")
    client = bigquery.Client(
    credentials=credentials,
    project=credentials.project_id)    
    print("Creating BQ Query table")
    dataset_ref = client.dataset(data_set, project = project_id )
    table_ref = dataset_ref.table(table_name)
    client.load_table_from_dataframe(df, table_ref).result()
    print("Table created successfully")
    