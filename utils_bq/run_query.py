from google.cloud import bigquery
from google.oauth2 import service_account
import pandas as pd

def query_to_big_query(query):
    """
    query: query que se quiere realizar a BQ
    """
    print("Set client connection")
    project_id = 'platzi-proyect-365801'
    credentials = service_account.Credentials.from_service_account_file(filename="platzi-proyect-365801-ec703f041c6b.json")
    print("Starting BQ Query")
    df_sql = pd.read_gbq(query, project_id=project_id, credentials=credentials)
    print("Returning df with query")
    return df_sql