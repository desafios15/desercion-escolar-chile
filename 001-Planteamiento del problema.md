# 001-Planteamiento del problema e hipótesis
# 1- Introducción:
# El lado “b” de la pandemia


En marzo del 2020, a nivel mundial, la pandemia obligó a los gobiernos a determinar el cierre de establecimientos educacionales, ya que representaban lugares de posible contagio a la población escolar. Hoy se estima que esta decisión pudo haber tenido graves impactos en la cultura y trayectoria de los estudiantes, entre ellas la *deserción escolar* de miles de niños, niñas y jóvenes que por diferentes motivos vieron dificultado el poder acceder a sus clases de manera remota, algo que habría afectado su motivación y continuidad en sus estudios.

En distintos estudios internacionales se menciona que el abandonar la educación formal escolar constituye un hito perjudicial que incrementa el riesgo de caer en una situación de pobreza y/o exclusión social (Eurostat, 2017). La educación formal “es un método que habilita a los sujetos para el ejercicio amplio de sus derechos” (Espinoza et al, 2014), de manera que el abandono escolar temprano es uno de los principales problemas para el éxito de las políticas educativas (Garcia, 2016).

Se ha investigado que el nivel socioeconómico del hogar y el nivel educacional de los padres se relaciona directamente con la probabilidad de que un estudiante complete su educación formal escolar (De Write et al, 2013). En Chile, poco se ha estudiado sobre la asociación entre la deserción escolar e indicadores socioeconómicos de ingresos, educación y salud, tales como el Índice de Prioridad Social (IPS).

Para hablar de deserción escolar es preciso considerar que como nos señala Christian Miranda, académico del Depto. de Educación de la Universidad Católica de Chile, “un estudiante que deserta es quien, en un determinado periodo académico, formando parte de un establecimiento educativo, deja de asistir, y no se matricula en otro establecimiento”

Según datos del MINEDUC en el año 2019 se estima que la tasa de prevalencia de estudiantes desertores fue de 289.115, con una tasa del 5.7% (Mineduc, 2019).

Del mismo modo, según proyecciones del MINEDUC del 2020, se espera que durante la pandemia (año 2020 en adelante) el número de desertores aumente en un 43%, debido a que una variable fuertemente asociada a la exclusión escolar es la asistencia. El ausentismo crónico, predispuesto por la pandemia, deriva en un peor desempeño académico e incrementa el riesgo de abuso de sustancias ilícitas y deserción del sistema (MINEDUC, 2020). 

>Mencionado lo anterior, el presente análisis busca resolver las siguientes interrogantes:
1. La pandemia en Chile, ¿ha aumentado la proporción de estudiantes en deserción escolar?
2. El efecto en la deserción escolar en la pandemia, ¿es distinto en comunas de distintos niveles de Índice de priorización social?


# 2- Objetivos:
- Conocer, analizar y medir el impacto que tuvo la pandemia respecto de la deserción escolar en la Región Metropolitana de Chile, comparando la situación antes (2017-2018) y durante la pandemia (2020-2021).
- Conocer la situación de deserción escolar antes (2017-2018) y durante la pandemia (2020-2021) en establecimientos educacionales de comunas con Alta Prioridad Social en la Región Metropolitana de Chile.
- Conocer la situación de deserción escolar antes (2017-2018) y durante la pandemia (2020-2021) en establecimientos educacionales de comunas Sin Prioridad Social en la Región Metropolitana de Chile.


# 3- Hipótesis de Investigación:

Hipótesis de invesgación 1:

- La proporción de estudiantes en deserción escolar ha aumentado desde que comenzó la pandemia.

Hipótesis de invesgación 2:

- La proporción de estudiantes en deserción escolar de establecimientos con ALTA PRIORIDAD SOCIAL ha aumentado desde que comenzó la pandemia.

Hipótesis de invesgación 2:

- La proporción de estudiantes en deserción escolar de establecimientos SIN PRIORIDAD SOCIAL ha disminuido desde que comenzó la pandemia.