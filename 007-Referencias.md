# 006- Referencias:

1- De Witte, K., Cabus, S., Thyssen, G., Groot, W., & van den Brink, H. M. (2013). A critical review of the literature on school dropout. Educational Research Review, 10, 13-28. doi: 10.1016/j.edurev.2013.05.002 

2- Espinoza, O., Castillo, D., González, L. E., Loyola, J., & Santa Cruz, E. (2014). Deserción escolar en Chile: un estudio de caso en relación con factores intraescolares. Educación y Educadores, 17(1), 32-50.

3- Eurostat (2017). Sustainable Development Indicators. Monitoring report on progress towards The SDGS in an EU context. Luxembourg. Recuperado de https://ec.europa.eu/eurostat/documents/3217494/8461633/KS-04-17-780-EN-N.pdf

4- García (2016). Indicadores de abandono escolar temprano: un marco para la reflexión sobre estrategias de mejora. Perfiles Educativos, XXXVIII, 154,191-213. Universidad Nacional Autónoma de México, Distrito Federal, México. Recuperado de http://www.redalyc.org/articulo.oa?id=13248313010 

5- MINEDUC (2020). Deserción escolar: diagnóstico y proyección en tiempo de pandemia. Centro de estudios MINEDUC. Documento de trabajo. Recuperado de https://centroestudios.mineduc.cl/wp-content/uploads/sites/100/2020/10/DOCUMENTO-DE-TRABAJO-22_2020_f01.pdf