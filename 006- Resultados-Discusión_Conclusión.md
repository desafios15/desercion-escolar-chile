# 006 Resultados, Discusión y Conclusiones

## 1- Resultados:

1.- La proporción de estudiantes retirados (desertores) PRE-PANDEMIA (2018-2019) es de un 3.28 % y durante la PANDEMIA (2020-2021) es de un 1.81 % (p-value = 0.0). 

    > Se rechaza la hipótesis de nulidad asociada a que la pandemia produce un aumento en la deserción escolar en los establecimientos de la región metropolitana de Chile.

2.- En comunas con Alta Prioridad Social la proporción de estudiantes retirados (desertores) PRE-PANDEMIA es de un 4.31 % y durante la PANDEMIA es de un 2.02 %  (p-value = 0.0).

     > Se rechaza la hipótesis de nulidad asociada a que la pandemia produce un aumento en la deserción escolar en los establecimientos de Alta prioridad Social en la región metropolitana de Chile.

3.- En comunas sin Prioridad Social la proporción de estudiantes retirados (desertores) PRE-PANDEMIA es de un 2.06 % y durante la PANDEMIA es de un 2.22 %  (p-value = 0.15)

     > Se rechaza la hipótesis de nulidad asociada a que la pandemia produce una disminución en la deserción escolar en los establecimientos Sin prioridad Social en la región metropolitana de Chile.
     
## 2- Conclusiones:

1.- Se observa que ha disminuido en un 45% la proporción de estudiantes desertores entre los periodos PRE-PANDEMIA y PANDEMIA.

2.- Los estudiantes de establecimientos educacionales con ALTA PRIORIDAD SOCIAL han disminuido en un 55% su proporción de deserción al comparar los periodos PRE-PANDEMIA y PANDEMIA.

3.- Los estudiantes de establecimientos educacionales SIN PRIORIDAD SOCIAL han aumentado en un 8% su proporción de deserción al comparar los periodos PRE-PANDEMIA y PANDEMIA.

## 3- Discusión:

Una posible explicación de los resultados encontrados (que es necesario indagar más, en otro momento) se podría relacionar a que los estudiantes y las familias durante la pandemia debieron permanecer en sus hogares de manera obligatoria más del tiempo que lo hacían previo a esta situación. Por supuesto, que siempre habrá excepciones a la regla (cómo toda varianza de cualquier métrica), pero se sabe y hay evidencia concluyente, de los efectos positivos que tienen los vínculos familiares en el desarrollo de los niños y adolescentes, y estos vínculos se podrían haber visto fortalecidos por la mayor cantidad de tiempo que pasaron los estudiantes dentro del hogar junto con sus padres y/o familiares.

El fortalecer este vínculo, ha generado una mejora en ciertas condiciones, por ejemplo la comunicación, no sienten abandono, ya que sus padres pasan mayor tiempo con ellos, y por último, tienen menor acceso a drogas y alcohol. Esto evita además que busquen distracción, y perdida de tiempo de sus obligaciones como estudiantes.

En Islandia, ya hace varios años se han implementado planes para enfrentar la drogadicción y el alcoholismo en adolescentes. Para ello decidieron empezar a medir muchas variables, tanto sociodemográficas, culturales etc. Luego de medir, cohortes de varios años, llegaron a tres importantes accionables:

1. Sugieren que los padres de los niños pasen cierta cantidad de tiempo diaria con sus hijos (se promueven políticas públicas para ello).
2. Impulsar las actividades extraprogramáticas en los estudiantes.
3. Toque de queda para adolescentes y niños.


Luego de implementado este plan, y de seguir midiendo, hoy en día Islandia puede señalar de manera concluyente de haber disminuido el alcoholismo y drogadicción en adolescentes desde un 45% a un 5%. Incluso aún, este plan cuyo nombre ha mutado a youth-in-europe, [planet-youth](https://planetyouth.org/), ha demostrado ser exitoso en varios países. En Chile, desde el año 2018 se comenzó a implementar en 6 comunas y también ha tenido [resultados alentadores](https://www.elmostrador.cl/agenda-pais/2021/05/19/modelo-islandes-de-prevencion-ha-permitido-que-comunas-bajen-sus-indices-de-consumo-de-marihuana-y-alcohol/).

Aquí algo importante y por ahora especulativo. La pandemia obligatoriamente ha promovido implementar parte del plan de Islandia durante dos años de manera intensiva. Los niños que por circunstancias de trabajo de sus padres no podían verlos todos los días, tuvieron la oportunidad de conocerlos más en su día a día. La pandemia también generó un toque de queda obligatorio (regla número tres del plan de Islandia).

La pandemia sin duda ha sido durísima para todo el planeta, ha causado un número significativo de muertes, problemas económicos e inestabilidad. Sin embargo, al parecer no todo es malo y es posible que haya sido una oportunidad para mejorar y fortalecer los vínculos familiares entre padres e hijos. ¿La pandemia tiene un lado B?

## 4- Pasos a seguir:

1. Evaluar la proporción de deserción escolar (alumnos retirados) por Género, Edad, Curso de los estudiantes y Dependencia de los establecimiento.

2. Trabajar en un análisis multivariado para evaluar cual de todas las variables es la más importante en la tasa de deserción escolar de la Región Metropolitana en Chile.

3. Incorporar datos a nivel país, segmentado por región, provincias y comunas.

4. Estudiar, ampliar las bibliografías y hablar con expertos que ayuden a interpretar los resultados y plantear hipótesis de asociación causal de los resultados encontrados en este análisis.

