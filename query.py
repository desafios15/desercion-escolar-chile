#Clase que contiene funciones con todas las queries usadas en el proyecto#

class Query:
    """
    Clase que contiene funciones con todas las queries usadas en el proyecto.
    """
        
    def query_rendimiento_rm():
        query = """
        select
            AGNO,
            RBD,
            NOM_RBD,
            COD_REG_RBD,
            COD_COM_RBD,
            NOM_COM_RBD,
            COD_DEPE2,
            COD_GRADO,
            MRUN,
            GEN_ALU,
            EDAD_ALU,
            PROM_GRAL,
            CAST(ASISTENCIA as integer) as ASISTENCIA,
            SIT_FIN_R,
            'PRE-PAND' as PANDEMIA
        from
            `rendimiento_escolar_chile.rendimiento_2018`
        WHERE
            COD_REG_RBD = 13
        UNION ALL
        select
            AGNO,
            RBD,
            NOM_RBD,
            COD_REG_RBD,
            COD_COM_RBD,
            NOM_COM_RBD,
            COD_DEPE2,
            COD_GRADO,
            MRUN,
            GEN_ALU,
            EDAD_ALU,
            PROM_GRAL,
            ASISTENCIA,
            SIT_FIN_R,
            'PRE-PAND' as PANDEMIA
        from
            `rendimiento_escolar_chile.rendimiento_2019`
        WHERE
            COD_REG_RBD = 13
        UNION ALL
        select
            AGNO,
            RBD,
            NOM_RBD,
            COD_REG_RBD,
            COD_COM_RBD,
            NOM_COM_RBD,
            COD_DEPE2,
            COD_GRADO,
            MRUN,
            GEN_ALU,
            EDAD_ALU,
            PROM_GRAL,
            ASISTENCIA,
            SIT_FIN_R,
            'PAND' as PANDEMIA
        from
            `rendimiento_escolar_chile.rendimiento_2020`
        WHERE
            COD_REG_RBD = 13
        UNION ALL
        select
            AGNO,
            RBD,
            NOM_RBD,
            COD_REG_RBD,
            COD_COM_RBD,
            NOM_COM_RBD,
            COD_DEPE2,
            COD_GRADO,
            MRUN,
            GEN_ALU,
            EDAD_ALU,
            PROM_GRAL,
            ASISTENCIA,
            SIT_FIN_R,
            'PAND' as PANDEMIA
        from
            `rendimiento_escolar_chile.rendimiento_2021`
        WHERE
            COD_REG_RBD = 13
        """
        return query

    def query_rendimiento_escolar_rm():
        query = """
        SELECT
            *
        FROM
            `temp_analysis.rendimiento_rm`
        """    
        return query
        
    def query_tasa_desercion_ips_rm():
        query = """
        with 
            tasa_desercion as(
                select 
                    AGNO,
                    RBD,
                    NOM_RBD,
                    COD_COM_RBD,
                    NOM_COM_RBD,
                    COD_DEPE2,
                    PANDEMIA,
                    count(*) as MATRICULA,
                    count(case when SIT_FIN_R = 'Y' then MRUN end) as Y,
                    count(case when SIT_FIN_R = 'P' then MRUN end) as P,
                    count(case when SIT_FIN_R = 'R' then MRUN end) as R,
                    count(case when SIT_FIN_R = 'T' then MRUN end) as T,
                    round((count(case when SIT_FIN_R = 'Y' then MRUN end)/count(*))*100,4) as DESER
                FROM
                    `temp_analysis.rendimiento_rm`
                GROUP BY 1,2,3,4,5,6,7)
        SELECT
            *
        FROM
            tasa_desercion as td
        LEFT JOIN
            `bases_junji.indice_prioridad_social` as ips on td.COD_COM_RBD=ips.COD_COM
        """
        return query
    
    def query_ips_rm():
        query = """
        SELECT 
            * 
        FROM 
            `platzi-proyect-365801.bases_junji.indice_prioridad_social`
        """
        return query
    
    def query_td_ips():
        query = """
        SELECT 
            * 
        FROM 
            `platzi-proyect-365801.temp_analysis.tasa_desercion_ips_rm`
        order by 7 desc
        """
        return query