# 000- Resumen

## 1. El lado "b" de la pandemia

La pandemia obligó a gobiernos a cerrar los establecimientos educacionales y a promover el acceso remoto a clases.

Según investigaciones publicadas por el Ministerio de Educación de Chile en el 2019, se conoce que la deserción escolar está fuertemente asociada a la asistencia a clases. Más aún, se describe que el ausentismo crónico, que en este caso predispone la pandemia, deriva en un peor desempeño académico e incrementa el riesgo de abuso de sustancias ilícitas y deserción del sistema (MINEDUC, 2020).     

>Mencionado lo anterior, el presente análisis busca resolver las siguientes interrogantes:
1. La pandemia en Chile, ¿ha aumentado la proporción de estudiantes en deserción escolar?
2. El efecto en la deserción escolar en la pandemia, ¿es homogeneo en comunas de distintos niveles de Índice de priorización social?

## 2. Objetivos:

- Conocer, analizar y medir el impacto que tuvo la pandemia respecto de la deserción escolar en la Región Metropolitana de Chile, comparando la situación antes (2017-2018) y durante la pandemia (2020-2021).
- Conocer la situación de deserción escolar antes (2017-2018) y durante la pandemia (2020-2021) en establecimientos educacionales de comunas con Alta Prioridad Social en la Región Metropolitana de Chile.
- Conocer la situación de deserción escolar antes (2017-2018) y durante la pandemia (2020-2021) en establecimientos educacionales de comunas Sin Prioridad Social en la Región Metropolitana de Chile.

## 3. ¿Cómo se busca responder la pregunta de investigación?

Para resolver estas interrogantes se utiliza información gubernamental disponible en la web (www.datosabiertos.mineduc.cl).

Se compara la media de las tasas de deserción escolar de estudiantes entre periodos Pre-Pandemia y Pandemia para conocer si el cierre de establecimientos tiene un efecto en la continuidad de estudios de los niños y adolescentes.

También, se compara la media de las tasas de deserción de estudiantes entre periodos Pre-Pandemia y Pandemia segmentados en comunas con Alta Priorización Social y Sin Priorización Social para evaluar si el desarrollo social (medida en dimensiones de ingreso, educación y salud de las familias) tiene un efecto en la deserción escolar en Chile.

## 4. Resultados:

>Obj 1: En la Figura 1 se observa que el promedio de la tasa de deserción de los establecimientos educacionales en el periodo PRE-PANDEMIA es de 3.28% y durante la PANDEMIA es de 1.81%. Esto representa una disminución de un 45% en la media de la tasa de deserción. 

Figura 1
![Obj_1](images/plot_obj_1.png)

>Obj 2 y 3: En la Figura 2 se observa que el promedio de la tasa de deserción de los establecimientos educacionales por nivel de Índice de Prioridad Social comunal en el periodo PRE-PANDEMIA y durante la PANDEMIA. Para los establecimientos de comunas con Alta Prioridad Social la tasa de deserción PRE-PANDEMIA es de 4.31% y durante la PANDEMIA de un 2.02%. Esto representa una disminución en la deserción de un 55%.

>Por su parte, en los establecimientos Sin Prioridad Social la tasa de deserción PRE-PANDEMIA tiene guarismos de 2.06% y durante la PANDEMIA de 2.22%. Esto representa un incremento en la tasa deserción de un 8 %.

Figura2:
![Obj_2](images/plot_obj_2_3.png)


## 5. Conclusión/discusión:

La pandemia, encierro en el hogar y modalidad de clases remoto muestra que tuvo un efecto positivo en la educación en Chile, diminuyendo las tasa de deserción en un 45% de los estudiantes de la Región Metropolitana de Chile. Por su parte, es muy interesante ver que en estudiantes de comunas Sin Priorización Social la situación no es la misma que a nivel Región. Es necesario seguir estudiando estos resultados para buscar posibles causas multidimensionales que aborden en mayor profundidad esta temática.